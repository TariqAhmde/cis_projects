﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Product Class
 Author: Tariq Ahmed
 Date: 12-07-2020
 CIS 2225 Final Practical
*/

namespace CIS2225_FinalPractical_Ahmed_Tariq.BusinessObjects
{
    public class Product
    {
        //private attributes
        private string description;
        private int retail;
        private int quantity;
        private int markup;
        private int productId;
        public Product(int productId)
        {
            ProductId = productId;
        }
        //default Product constructor
        public Product()
        {

        }

        public string Description { get => description; set => description = value; }
        public int Retail { get => retail; set => retail = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public int Markup { get => markup; set => markup = value; }
        public int ProductId { get => productId; set => productId = value; }
       
        //override toString to display productId attibute value
        public override string ToString()
        {
            return "" + ProductId;
        }
    }
}
