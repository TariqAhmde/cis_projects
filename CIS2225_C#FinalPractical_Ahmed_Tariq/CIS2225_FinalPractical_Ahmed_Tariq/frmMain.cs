﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using CIS2225_FinalPractical_Ahmed_Tariq.BusinessObjects;

/* Product Main Form
 Author: Tariq Ahmed
 Date: 12-07-2020
 CIS 2225 Final Practical
*/

namespace CIS2225_FinalPractical_Ahmed_Tariq
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        //build connection to Product
        //create connection string for Product database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";

        //Create OldDbConnection
        OleDbConnection dbConn;

        private void frmMain_Load(object sender, EventArgs e)
        {
            PopulateProductCombo();
        }
        private void PopulateProductCombo()
        {
            //Clear any items in combo box
            cbIdSelector.Items.Clear();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Product table
                string sql;
                sql = "SELECT Product_ID from Product;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();
                //Read first record
                while (dbReader.Read())
                {
                    //Create a product object to populate the productId attibute
                    Product product = new Product((int)dbReader["Product_ID"]);

                    //load the ProductID object per into the combobox
                    //when displayed the combo box will call toString by default on the Product object.
                    //the toString only displays the ProductID of the product.
                    cbIdSelector.Items.Add(product);
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void PopulateProduct()
        {
            //the combobox is populated with Product objects. 

           int productSelection = ((Product)cbIdSelector.SelectedItem).ProductId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Product table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the ProductID to search for
                sql = "SELECT(Select count(Product_ID) from Product where Product_ID = " + productSelection + ") " +
                        "as rowCount, * from Product where Product_ID = " + productSelection + ";"; 
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes
                    txtDescription.Text = dbReader["Description"].ToString();
                    txtRetailPrice.Text = dbReader["Retail_Price"].ToString();
                    txtQuantity.Text = dbReader["Quantity"].ToString();
                    txtMarkUp.Text = dbReader["MarkUp"].ToString();
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cbIdSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Call method to populate the form based off the selection from cbPersonSelector
            PopulateProduct();
        }

        // Clear Button Functionality
        private void btnClear_Click(object sender, EventArgs e)
        {
            //Refresh Person selector combo box
            PopulateProductCombo();
            //Clear the form
            ClearForm();
        }
        // Insert Button Functionality
        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {

                //build connection to Product
                //create connection string for Product database
                string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";
                //Create OldDbConnection
                OleDbConnection dbConn;
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Person table
                    string sql;
                    sql = "Insert into Product(Description, Retail_Price, Quantity, MarkUp) Values (@Description, @Retail_Price, @Quantity, @MarkUp);"; //note the two semicolons

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    dbCmd.Parameters.AddWithValue("@Retail_Price", txtRetailPrice.Text);
                    dbCmd.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
                    dbCmd.Parameters.AddWithValue("@MarkUp", txtMarkUp.Text);

                    //execute insert. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();
                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record inserted successfully");
                        //Refresh Product selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();

                    }
                    else
                    {
                        MessageBox.Show("Error inserting record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }
        // Form Validation
        private bool ValidateForm()
        {
            string errMsg = "";
            if (txtDescription.Text == "")
            {
                errMsg = "Missing Description. \n";
            }
            if (!(int.TryParse(txtRetailPrice.Text, out int retail)))
            {
                errMsg = "Retail Price Missing or Incorrect. \n";
            }
            if (!(int.TryParse(txtQuantity.Text, out int quantity)))
            {
                errMsg = "Quantity Missing or Incorrect. \n";
            }
            if (!(int.TryParse(txtMarkUp.Text, out int markup))) 
            {
                errMsg = "Markup Missing or Incorrect. \n";
            }
            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        // Clear Text Boxes 
        private void ClearForm()
        {
            //Clear text boxes
            txtDescription.Text = "";
            txtMarkUp.Text = "";
            txtQuantity.Text = "";
            txtRetailPrice.Text = "";
        }

        // Delete Button Functionality
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want delete this record?", "Delete Product",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //delete the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to delete selected product record
                    string sql;
                    sql = "Delete from Product where Product_ID = @Product_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Product_ID", cbIdSelector.Text);

                    //execute delete. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {
                        
                        MessageBox.Show("Product deleted successfully");
                        //Refresh PersoProductn selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();
                }
                    
                    else
                    {
                        MessageBox.Show("Error deleting record. Please try again.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //close database connection
                    dbConn.Close();
                }
            }
        }
    }
}
