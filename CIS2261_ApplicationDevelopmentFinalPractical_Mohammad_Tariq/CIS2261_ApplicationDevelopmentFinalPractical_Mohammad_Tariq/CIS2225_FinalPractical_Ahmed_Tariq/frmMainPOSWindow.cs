﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq
{ 
    public partial class frmMainPOSWindow : Form
{


    public frmMainPOSWindow()
    {
        InitializeComponent();
        panel1.Visible = false;
    }

    private void button1_Click(object sender, EventArgs e)
    {
        this.Close();
    }

    private void btnNewSale_Click(object sender, EventArgs e)
    {
        var customer = new frmCustomer();

        panel1.Visible = true;
        lblWelcome.Visible = false;


    }

    private void frmMainPOSWindow_Load(object sender, EventArgs e)
    {

    }

    private void btnCustomer_Click(object sender, EventArgs e)
    {
        var customer = new frmCustomer();
        customer.Show();


    }

    private void btnProducts_Click(object sender, EventArgs e)
    {

    }

    private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
    {
        var customer = new frmCustomer();
        //Call method to populate the form based off the selection from cbPersonSelector
        //customer.PopulateCustomer();
    }

    private void btnPayNow_Click(object sender, EventArgs e)
    {
        var payment = new frmPayment();
        payment.ShowDialog();
    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }
  }
}
