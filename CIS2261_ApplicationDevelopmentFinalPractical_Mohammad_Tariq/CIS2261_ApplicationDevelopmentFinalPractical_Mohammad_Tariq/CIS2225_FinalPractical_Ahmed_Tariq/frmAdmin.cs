﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            
            InitializeComponent();
        }
        string username;
        public frmAdmin(String s)
        {

            InitializeComponent();
            username = s;
        }
        private void frmAdmin_Load(object sender, EventArgs e)
        {
             if (username == "staff")
            {
                btnStaffReg.Hide();
                btnSupplier.Hide();
                btnManage.Hide();
            }
        }

        private void btnStaffReg_Click(object sender, EventArgs e)
        {
            frmStaffRegistration staff = new frmStaffRegistration();
            staff.Show();
        }

        private void btnClient_Click(object sender, EventArgs e)
        {
            frmCustomer customer = new frmCustomer();
            customer.Show();

        }

        private void btnManage_Click(object sender, EventArgs e)
        {
            frmProduct product = new frmProduct();
            product.Show();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            frmSupplier supplier = new frmSupplier();
            supplier.Show();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnServices_Click(object sender, EventArgs e)
        {
            frmMainPOSWindow pos = new frmMainPOSWindow();
            pos.Show();
        }

        private void btnSalesReport_Click(object sender, EventArgs e)
        {
            frmSalesReport salesReport = new frmSalesReport();
            salesReport.Show();
        }
    }
}
