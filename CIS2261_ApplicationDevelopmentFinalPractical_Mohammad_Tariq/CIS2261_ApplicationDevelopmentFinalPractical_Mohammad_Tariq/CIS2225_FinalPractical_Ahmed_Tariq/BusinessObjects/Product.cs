﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Product Class
 Author: Tariq Ahmed
 Date: 12-07-2020
 CIS 2225 Final Practical
*/

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects
{
    public class Product
    {
        //private attributes
        private string productName;
        private string group;
        private string category;
        private string productDesc;
        private string manufacturer;
        private string batchNo;
        private string manufacturerDate;
        private string entryDate;
        private string expireDate;
        private string quantity;
        private string costPrice;
        private string totalPrice;
        private int supplierId;
        private int productId;
        public Product(int productId)
        {
            ProductId = productId;
        }
        //default Product constructor
        public Product()
        {

        }

       
        public int ProductId { get => productId; set => productId = value; }
        public string ProductName { get => productName; set => productName = value; }
        public string Group { get => group; set => group = value; }
        public string Category { get => category; set => category = value; }
        public string ProductDesc { get => productDesc; set => productDesc = value; }
        public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        public string BatchNo { get => batchNo; set => batchNo = value; }
        public string ManufacturerDate { get => manufacturerDate; set => manufacturerDate = value; }
        public string EntryDate { get => entryDate; set => entryDate = value; }
        public string ExpireDate { get => expireDate; set => expireDate = value; }
        public string Quantity { get => quantity; set => quantity = value; }
        public string CostPrice { get => costPrice; set => costPrice = value; }
        public string TotalPrice { get => totalPrice; set => totalPrice = value; }
        public int SupplierId { get => supplierId; set => supplierId = value; }

        //override toString to display productId attibute value
        public override string ToString()
        {
            return "" + ProductId;
        }
    }
}
