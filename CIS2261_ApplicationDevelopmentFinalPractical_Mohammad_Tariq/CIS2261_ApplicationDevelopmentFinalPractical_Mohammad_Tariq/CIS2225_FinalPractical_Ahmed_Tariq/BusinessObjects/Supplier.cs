﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects
{
    class Supplier
    {
        //private attributes
        private string firstName;
        private string lastName;
        private string company;
        private string address;
        private string phone;
        private string email;
        private int supplierId;
        public Supplier(int supplierId)
        {
            SupplierId = supplierId;
        }
        //default Product constructor
        public Supplier()
        {

        }

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Company { get => company; set => company = value; }
        public string Address { get => address; set => address = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }
        public int SupplierId { get => supplierId; set => supplierId = value; }

        //override toString to display productId attibute value
        public override string ToString()
        {
            return "" + SupplierId;
        }
    }
}

