﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects

{
    class Staff
    {
        //private attributes
       
        private int staffId;
        private string firstName;
        private string lastName;
        private string address;
        private string city;
        private string province;
        private string postal;
        private string email;
        private string phone;
        private string joinDate;
        private string dob;
        private string username;
        private string password;

        public Staff(int staffId)
        {
            StaffId = staffId;
        }
        //default Product constructor
        public Staff()
        {

        }
        public int StaffId { get => staffId; set => staffId = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string City { get => city; set => city = value; }
        public string Address { get => address; set => address = value; }
        public string Province { get => province; set => province = value; }
        public string Postal { get => postal; set => postal = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }
        public string JoinDate { get => joinDate; set => joinDate = value; }
        public string Dob { get => dob; set => dob = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }


       

        //override toString to display productId attibute value
        public override string ToString()
        {
            return "" + StaffId;
        }

    }
}
