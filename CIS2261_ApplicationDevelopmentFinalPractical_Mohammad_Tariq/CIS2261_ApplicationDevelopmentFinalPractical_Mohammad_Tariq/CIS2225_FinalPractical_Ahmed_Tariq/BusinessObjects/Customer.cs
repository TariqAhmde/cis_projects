﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects

{
    class Customer
    {
        //private attributes
       
        private int customerId;
        private string firstName;
        private string lastName;
        private string company;
        private string address;
        private string phone;
        private string email;

        public Customer(int customerId)
        {
            CustomerId = customerId;
        }
        //default Product constructor
        public Customer()
        {

        }
        public int CustomerId { get => customerId; set => customerId = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Company { get => company; set => company = value; }
        public string Address { get => address; set => address = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }


       

        //override toString to display productId attibute value
        public override string ToString()
        {
            return "" + CustomerId;
        }

    }
}
