﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects;
namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq
{
    public partial class frmCustomer : Form
    {
        public frmCustomer()
        {
            InitializeComponent();
        }

        //build connection to Product
        //create connection string for Product database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";

        //Create OldDbConnection
        OleDbConnection dbConn;

        private void frmCustomer_Load(object sender, EventArgs e)
        {
                 PopulateProductCombo();
        }
            private void PopulateProductCombo()
            {
                //Clear any items in combo box
                cbIdSelector.Items.Clear();

                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Product table
                    string sql;
                    sql = "SELECT Customer_ID from Customer;"; //note the two semicolons

                    OleDbCommand dbCmd = new OleDbCommand();
                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;
                    //create OleDbDataReader dbReader
                    OleDbDataReader dbReader;
                    //Read data into dbReader
                    dbReader = dbCmd.ExecuteReader();
                    //Read first record
                    while (dbReader.Read())
                    {
                        //Create a product object to populate the productId attibute
                        Customer customer = new Customer((int)dbReader["Customer_ID"]);

                        //load the ProductID object per into the combobox
                        //when displayed the combo box will call toString by default on the Product object.
                        //the toString only displays the ProductID of the product.
                        cbIdSelector.Items.Add(customer);
                    }
                    //close Reader
                    dbReader.Close();
                    //close database connection
                    dbConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            private void PopulateCustomer()
            {
                //the combobox is populated with Product objects. 

                int customerSelection = ((Customer)cbIdSelector.SelectedItem).CustomerId;

                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Product table
                    string sql;

                    //Add a subquery to get the record count
                    //Using a variable to identify the ProductID to search for
                    sql = "SELECT(Select count(Customer_ID) from Customer where Customer_ID = " + customerSelection + ") " +
                            "as rowCount, * from Customer where Customer_ID = " + customerSelection + ";";
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;

                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //get number of rows
                    //ExecuteScalar just returns the value of the first column
                    int numRows = (Int32)dbCmd.ExecuteScalar();

                    //create OleDbDataReader dbReader
                    OleDbDataReader dbReader;

                    //Read data into dbReader
                    dbReader = dbCmd.ExecuteReader();

                    //Read first record
                    dbReader.Read();
                    if (dbReader.HasRows && numRows == 1)
                    {
                        //get data from dbReader by column name and assing to text boxes
                        txtLastName.Text = dbReader["Last_Name"].ToString();
                        txtFirstName.Text = dbReader["First_Name"].ToString();
                        txtPhoneNo.Text = dbReader["Phone"].ToString();
                        txtCompanyName.Text = dbReader["Company"].ToString();
                        txtAddress.Text = dbReader["Address"].ToString();
                        txtEmail.Text = dbReader["Email"].ToString();
                    }
                    //Close open connections
                    dbReader.Close();
                    dbConn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            // Insert Button Functionality
            private void btnInsert_Click(object sender, EventArgs e)
            {
              
            }
            // Clear Text Boxes 
        

            // Delete Button Functionality
 
        private void btnDeleteSupplier_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want delete this record?", "Delete Customer",
               MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //delete the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to delete selected product record
                    string sql;
                    sql = "Delete from Customer where Customer_ID = @Customer_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Customer_ID", cbIdSelector.Text);

                    //execute delete. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {

                        MessageBox.Show("Customer deleted successfully");
                        //Refresh PersoProductn selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();
                    }

                    else
                    {
                        MessageBox.Show("Error deleting record. Please try again.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //close database connection
                    dbConn.Close();
                }
            }
        }

        private void btnEditSupplier_Click(object sender, EventArgs e)
        {
            //Check to see if they want to update the curren record
            DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //update the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to update selected person record
                    string sql;
                    sql = "Update Customer set First_Name = @First_Name, Last_Name = @Last_Name, Company = @Company, Address = @Address, Phone = @Phone, Email = @Email where Customer_ID = @Customer_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@First_Name", txtFirstName.Text);
                    dbCmd.Parameters.AddWithValue("@Last_Name", txtLastName.Text);
                    dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    dbCmd.Parameters.AddWithValue("@Company", txtCompanyName.Text);
                    dbCmd.Parameters.AddWithValue("@Phone", txtPhoneNo.Text);
                    dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    dbCmd.Parameters.AddWithValue("@Customer_ID", cbIdSelector.Text);

                    //execute update. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record updated successfully");
                    }
                    else
                    {
                        MessageBox.Show("Error updating record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btnAddSupplier_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {

                //build connection to Product
                //create connection string for Product database
                string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";
                //Create OldDbConnection
                OleDbConnection dbConn;
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Person table
                    string sql;
                    sql = "Insert into Customer(First_Name, Last_Name, Company, Address, Phone, Email) Values (@First_Name, @Last_Name, @Company, @Address, @Phone, @Email);"; //note the two semicolons

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@First_Name", txtFirstName.Text);
                    dbCmd.Parameters.AddWithValue("@Last_Name", txtLastName.Text);
                    dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    dbCmd.Parameters.AddWithValue("@Company", txtCompanyName.Text);
                    dbCmd.Parameters.AddWithValue("@Phone", txtPhoneNo.Text);
                    dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);

                    //execute insert. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();
                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record inserted successfully");
                        //Refresh Product selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();

                    }
                    else
                    {
                        MessageBox.Show("Error inserting record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }
        // Form Validation
        private bool ValidateForm()
        {
            string errMsg = "";
            if (txtFirstName.Text == "")
            {
                errMsg = "Missing First Name. \n";
            }
            if (txtLastName.Text == "")
            {
                errMsg = "Missing Last Name. \n";
            }
            if (txtCompanyName.Text == "")
            {
                errMsg = "Missing Company Name. \n";
            }
            if (txtAddress.Text == "")
            {
                errMsg = "Missing Address. \n";
            }
            if (txtPhoneNo.Text == "")
            {
                errMsg = "Missing Phone No. \n";
            }
            if (txtEmail.Text == "")
            {
                errMsg = "Missing Email. \n";
            }


            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        // Clear Text Boxes 
        private void ClearForm()
        {
            //Clear text boxes
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPhoneNo.Text = "";
            txtCompanyName.Text = "";
            txtAddress.Text = "";
        }

    

        private void cbIdSelector_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Call method to populate the form based off the selection from cbPersonSelector
            PopulateCustomer();
        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            //Refresh Person selector combo box
            PopulateProductCombo();
            //Clear the form
            ClearForm();
        }
    }
    }

