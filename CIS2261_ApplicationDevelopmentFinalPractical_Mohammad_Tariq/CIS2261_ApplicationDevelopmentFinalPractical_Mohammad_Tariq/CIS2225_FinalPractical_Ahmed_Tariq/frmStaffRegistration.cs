﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects;
namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq
{
    public partial class frmStaffRegistration : Form
    {
        public frmStaffRegistration()
        {
            InitializeComponent();
        }
        //build connection to Product
        //create connection string for Product database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";

        //Create OldDbConnection
        OleDbConnection dbConn;

        private void frmStaffRegistration_Load(object sender, EventArgs e)
        {
            PopulateStaffCombo();
        }
        private void PopulateStaffCombo()
        {
            //Clear any items in combo box
            cbIdSelector.Items.Clear();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Staff table
                string sql;
                sql = "SELECT Staff_ID from Staff;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();
                //Read first record
                while (dbReader.Read())
                {
                    //Create a product object to populate the staffId attibute
                    Staff staff = new Staff((int)dbReader["Staff_ID"]);

                    //load the staffId object per into the combobox
                    //when displayed the combo box will call toString by default on the satff object.
                    //the toString only displays the staffId of the staff.
                    cbIdSelector.Items.Add(staff);
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void PopulateStaff()
        {
            //the combobox is populated with Staff objects. 

            int staffSelection = ((Staff)cbIdSelector.SelectedItem).StaffId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Staff table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the ProductID to search for
                sql = "SELECT(Select count(Staff_ID) from Staff where Staff_ID = " + staffSelection + ") " +
                        "as rowCount, * from Staff where Staff_ID = " + staffSelection + ";";
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes
                    txtLastName.Text = dbReader["Last_Name"].ToString();
                    txtFirstName.Text = dbReader["First_Name"].ToString();
                    txtPhoneNo.Text = dbReader["Phone"].ToString();
                    txtCity.Text = dbReader["City"].ToString();
                    txtPostalCode.Text = dbReader["Postal"].ToString();
                    txtAddress.Text = dbReader["Address"].ToString();
                    txtProvince.Text = dbReader["Province"].ToString();
                    txtEmail.Text = dbReader["Email"].ToString();
                    txtJoiningDate.Text = dbReader["Join_Date"].ToString();
                    txtDob.Text = dbReader["Dob"].ToString();
                    txtUser.Text = dbReader["Username"].ToString();
                    txtPass.Text = dbReader["Password"].ToString();
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

       

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {

                //build connection to Product
                //create connection string for Product database
                string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";
                //Create OldDbConnection
                OleDbConnection dbConn;
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Person table
                    string sql;
                    sql = "Insert into Staff(Last_Name, First_Name, Address, City, Province, Postal, Email, Phone, Join_Date, Dob, Username, [Password]) Values (@Last_Name, @First_Name, @Address, @City, @Province, @Postal, @Email, @Phone, @Join_Date, @Dob, @Username, @Password);"; //note the two semicolons

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Last_Name", txtLastName.Text);
                    dbCmd.Parameters.AddWithValue("@First_Name", txtFirstName.Text);
                    dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    dbCmd.Parameters.AddWithValue("@City", txtCity.Text);
                    dbCmd.Parameters.AddWithValue("@Province", txtProvince.Text);
                    dbCmd.Parameters.AddWithValue("@Postal", txtPostalCode.Text);
                    dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    dbCmd.Parameters.AddWithValue("@Phone", txtPhoneNo.Text);
                    dbCmd.Parameters.AddWithValue("@Join_Date", txtJoiningDate.Text);
                    dbCmd.Parameters.AddWithValue("@Dob", txtDob.Text);
                    dbCmd.Parameters.AddWithValue("@Username", txtUser.Text);
                    dbCmd.Parameters.AddWithValue("@Password", txtPass.Text);

                    //execute insert. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();
                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record inserted successfully");
                        //Refresh Product selector combo box
                        PopulateStaffCombo();
                        //Clear the form
                        ClearForm();

                    }
                    else
                    {
                        MessageBox.Show("Error inserting record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Check to see if they want to update the curren record
            DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //update the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to update selected staff record
                    string sql;
                    sql = "Update Staff set First_Name = @First_Name, Last_Name = @Last_Name, City = @City, Address = @Address, Province = @Province, Postal = @Postal, Email = @Email, Phone = @Phone, Join_Date = @Join_Date, Dob = @Dob, Username = @Username, [Password] = @Password where Staff_ID = @Staff_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Last_Name", txtLastName.Text);
                    dbCmd.Parameters.AddWithValue("@First_Name", txtFirstName.Text);
                    dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    dbCmd.Parameters.AddWithValue("@City", txtCity.Text);
                    dbCmd.Parameters.AddWithValue("@Province", txtProvince.Text);
                    dbCmd.Parameters.AddWithValue("@Postal", txtPostalCode.Text);
                    dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    dbCmd.Parameters.AddWithValue("@Phone", txtPhoneNo.Text);
                    dbCmd.Parameters.AddWithValue("@Join_Date", txtJoiningDate.Text);
                    dbCmd.Parameters.AddWithValue("@Dob", txtDob.Text);
                    dbCmd.Parameters.AddWithValue("@Username", txtUser.Text);
                    dbCmd.Parameters.AddWithValue("@Password", txtPass.Text);
                    dbCmd.Parameters.AddWithValue("@Staff_ID", cbIdSelector.Text);

                    //execute update. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record updated successfully");
                    }
                    else
                    {
                        MessageBox.Show("Error updating record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want delete this record?", "Delete Staff",
              MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //delete the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to delete selected product record
                    string sql;
                    sql = "Delete from Staff where Staff_ID = @Staff_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Staff_ID", cbIdSelector.Text);

                    //execute delete. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {

                        MessageBox.Show("Customer deleted successfully");
                        //Refresh PersoProductn selector combo box
                        PopulateStaffCombo();
                        //Clear the form
                        ClearForm();
                    }

                    else
                    {
                        MessageBox.Show("Error deleting record. Please try again.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //close database connection
                    dbConn.Close();
                }
            }
        }
        // Form Validation
        private bool ValidateForm()
        {
            string errMsg = "";
            if (txtFirstName.Text == "")
            {
                errMsg = "Missing First Name. \n";
            }
            if (txtLastName.Text == "")
            {
                errMsg = "Missing Last Name. \n";
            }
            if (txtCity.Text == "")
            {
                errMsg = "Missing City Name. \n";
            }
            if (txtAddress.Text == "")
            {
                errMsg = "Missing Address. \n";
            } 
            if (txtProvince.Text == "")
            {
                errMsg = "Missing Province. \n";
            } 
            if (txtPostalCode.Text == "")
            {
                errMsg = "Missing Postal Code. \n";
            }
            if (txtPhoneNo.Text == "")
            {
                errMsg = "Missing Phone No. \n";
            }
            if (txtEmail.Text == "")
            {
                errMsg = "Missing Email. \n";
            }
             if (txtJoiningDate.Text == "")
            {
                errMsg = "Missing Joining Date. \n";
            }
             if (txtDob.Text == "")
            {
                errMsg = "Missing Date Of Birth. \n";
            }
             if (txtUser.Text == "")
            {
                errMsg = "Missing Username. \n";
            }
             if (txtPass.Text == "")
            {
                errMsg = "Missing Password. \n";
            }


            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        // Clear Text Boxes 
        private void ClearForm()
        {
            //Clear text boxes
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPhoneNo.Text = "";
            txtJoiningDate.Text = "";
            txtAddress.Text = "";
            txtPostalCode.Text = "";
            txtDob.Text = "";
            txtCity.Text = "";
            txtUser.Text = "";
            txtProvince.Text = "";
            txtPass.Text = "";
        }
        private void cbIdSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Call method to populate the form based off the selection from cbPersonSelector

            PopulateStaff();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //Refresh Staff selector combo box
            PopulateStaffCombo();
            //Clear the form
            ClearForm();
        }
    }
}
