﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using CIS2261_Application_Development_Final_Project_Mohammad_Tariq.BusinessObjects;

/* 
 Product Main Form
 Author: Tariq Ahmed
 Date: 2021-03-12
 
*/

namespace CIS2261_Application_Development_Final_Project_Mohammad_Tariq
{
    public partial class frmProduct : Form
    {
        public frmProduct()
        {
            InitializeComponent();
            
        }
      
        //build connection to Product
        //create connection string for Product database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";

        //Create OldDbConnection
        OleDbConnection dbConn;

        private void frmMain_Load(object sender, EventArgs e)
        {
            PopulateProductCombo();
        }
        private void PopulateProductCombo()
        {
            //Clear any items in combo box
            cbIdSelector.Items.Clear();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Product table
                string sql;
                sql = "SELECT Product_ID from Product;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();
                //Read first record
                while (dbReader.Read())
                {
                    //Create a product object to populate the productId attibute
                    Product product = new Product((int)dbReader["Product_ID"]);

                    //load the ProductID object per into the combobox
                    //when displayed the combo box will call toString by default on the Product object.
                    //the toString only displays the ProductID of the product.
                    cbIdSelector.Items.Add(product);
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void PopulateProduct()
        {
            //the combobox is populated with Product objects. 

           int productSelection = ((Product)cbIdSelector.SelectedItem).ProductId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Product table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the ProductID to search for
                sql = "SELECT(Select count(Product_ID) from Product where Product_ID = " + productSelection + ") " +
                        "as rowCount, * from Product where Product_ID = " + productSelection + ";"; 
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes
                    txtProductName.Text = dbReader["ProductName"].ToString();
                    txtGroup.Text = dbReader["ProductGroup"].ToString();
                    txtCategory.Text = dbReader["ProductCategory"].ToString();
                    txtProductDescription.Text = dbReader["ProductDescription"].ToString();
                    txtManufacturer.Text = dbReader["ManufacturerName"].ToString();
                    txtBatch.Text = dbReader["BatchNumber"].ToString();
                    txtManuDate.Text = dbReader["ManufacturerDate"].ToString();
                    txtEntryDate.Text = dbReader["ProductEntryDate"].ToString();
                    txtExpiry.Text = dbReader["ExpireDate"].ToString();
                    txtQuantity.Text = dbReader["ProductQuantity"].ToString();
                    txtCost.Text = dbReader["ProductCostPrice"].ToString();
                    txtTotal.Text = dbReader["ProductTotalPrice"].ToString();
                    txtSupplierId.Text = dbReader["SupplierId"].ToString();
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cbIdSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Call method to populate the form based off the selection from cbPersonSelector
            PopulateProduct();
        }

        // Clear Button Functionality
        private void btnClear_Click(object sender, EventArgs e)
        {
            //Refresh Person selector combo box
            PopulateProductCombo();
            //Clear the form
            ClearForm();
        }
     
        // Form Validation
        private bool ValidateForm()
        {
            string errMsg = "";
            if (txtProductName.Text == "")
            {
                errMsg = "Missing Product Name. \n";
            }
           if (txtBatch.Text == "")
            {
                errMsg = "Missing Batch No. \n";
            }
           if (txtCategory.Text == "")
            {
                errMsg = "Missing Category Name. \n";
            }
           if (txtCost.Text == "")
            {
                errMsg = "Missing Cost. \n";
            }
           if (txtEntryDate.Text == "")
            {
                errMsg = "Missing Entry Date. \n";
            }
           if (txtExpiry.Text == "")
            {
                errMsg = "Missing Expiry Date. \n";
            }
           if (txtGroup.Text == "")
            {
                errMsg = "Missing group. \n";
            }
           if (txtManuDate.Text == "")
            {
                errMsg = "Missing Manufacturer Date. \n";
            }
           if (txtManufacturer.Text == "")
            {
                errMsg = "Missing Manufacturer Name. \n";
            }
           if (txtProductDescription.Text == "")
            {
                errMsg = "Missing product description. \n";
            }
           if (txtQuantity.Text == "")
            {
                errMsg = "Missing quantity. \n";
            }
           if (txtSupplierId.Text == "")
            {
                errMsg = "Missing supplier id. \n";
            }
           if (txtTotal.Text == "")
            {
                errMsg = "Missing Total Cost. \n";
            }
           
            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        // Clear Text Boxes 
        private void ClearForm()
        {
            //Clear text boxes
            txtBatch.Text = "";
            txtCategory.Text = "";
            txtCost.Text = "";
            txtEntryDate.Text = "";
            txtExpiry.Text = "";
            txtGroup.Text = "";
            txtManuDate.Text = "";
            txtManufacturer.Text = "";
            txtProductDescription.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
            txtSupplierId.Text = "";
            txtTotal.Text = "";
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void lblSupplierName_Click(object sender, EventArgs e)
        {

        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {

                //build connection to Product
                //create connection string for Product database
                string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=Product.accdb";
                //Create OldDbConnection
                OleDbConnection dbConn;
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to select all rows from Person table
                    string sql;
                    sql = "Insert into Product(ProductName, ProductGroup, ProductCategory, ProductDescription, ManufacturerName, " +
                        "BatchNumber, ManufacturerDate, ProductEntryDate, ExpireDate, ProductQuantity, ProductCostPrice, ProductTotalPrice, " +
                        "SupplierId) Values (@ProductName, @ProductGroup, @ProductCategory, @ProductDescription, @ManufacturerName, " +
                        "@BatchNumber, @ManufacturerDate, @ProductEntryDate, @ExpireDate, @ProductQuantity, @ProductCostPrice, @ProductTotalPrice, " +
                        "@SupplierId);"; //note the two semicolons

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@ProductName", txtProductName.Text);
                    dbCmd.Parameters.AddWithValue("@ProductGroup", txtGroup.Text);
                    dbCmd.Parameters.AddWithValue("@ProductCategory", txtCategory.Text);
                    dbCmd.Parameters.AddWithValue("@ProductDescription", txtProductName.Text);
                    dbCmd.Parameters.AddWithValue("@ManufacturerName", txtManufacturer.Text);
                    dbCmd.Parameters.AddWithValue("@BatchNumber", txtBatch.Text);
                    dbCmd.Parameters.AddWithValue("@ManufacturerDate", txtManuDate.Text);
                    dbCmd.Parameters.AddWithValue("@ProductEntryDate", txtEntryDate.Text);
                    dbCmd.Parameters.AddWithValue("@ExpireDate", txtExpiry.Text);
                    dbCmd.Parameters.AddWithValue("@ProductQuantity", txtExpiry.Text);
                    dbCmd.Parameters.AddWithValue("@ProductCostPrice", txtCost.Text);
                    dbCmd.Parameters.AddWithValue("@ProductTotalPrice", txtTotal.Text);
                    dbCmd.Parameters.AddWithValue("@SupplierId", txtSupplierId.Text);

                    //execute insert. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();
                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record inserted successfully");
                        //Refresh Product selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();

                    }
                    else
                    {
                        MessageBox.Show("Error inserting record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            //Refresh Person selector combo box
            PopulateProductCombo();
            //Clear the form
            ClearForm();
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want delete this record?", "Delete Product",
               MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //delete the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to delete selected product record
                    string sql;
                    sql = "Delete from Product where Product_ID = @Product_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@Product_ID", cbIdSelector.Text);

                    //execute delete. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {

                        MessageBox.Show("Product deleted successfully");
                        //Refresh PersoProductn selector combo box
                        PopulateProductCombo();
                        //Clear the form
                        ClearForm();
                    }

                    else
                    {
                        MessageBox.Show("Error deleting record. Please try again.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //close database connection
                    dbConn.Close();
                }
            }
        }

        private void btnEditProduct_Click(object sender, EventArgs e)
        {
            //Check to see if they want to update the current record
            DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //update the record
                try
                {
                    dbConn = new OleDbConnection(sConnection);
                    //open connection to database
                    dbConn.Open();
                    //create query to update selected person record
                    string sql;
                    sql = "Update Product set ProductName = @ProductName, ProductGroup = @ProductGroup, ProductCategory " +
                        "= @ProductCategory, ProductDescription = @ProductDescription, ManufacturerName = @ManufacturerName, " +
                        "BatchNumber = @BatchNumber, ManufacturerDate = @ManufacturerDate, ProductEntryDate = @ProductEntryDate, " +
                        "ExpireDate = @ExpireDate, ProductQuantity = @ProductQuantity, ProductCostPrice = @ProductCostPrice, " +
                        "ProductTotalPrice = @ProductTotalPrice, SupplierId = @SupplierId where Product_ID = @Product_ID";

                    //create database command
                    OleDbCommand dbCmd = new OleDbCommand();

                    //set command SQL string
                    dbCmd.CommandText = sql;
                    //set the command connection
                    dbCmd.Connection = dbConn;

                    //bind parameters
                    //bind parameters
                    dbCmd.Parameters.AddWithValue("@ProductName", txtProductName.Text);
                    dbCmd.Parameters.AddWithValue("@ProductGroup", txtGroup.Text);
                    dbCmd.Parameters.AddWithValue("@ProductCategory", txtCategory.Text);
                    dbCmd.Parameters.AddWithValue("@ProductDescription", txtProductName.Text);
                    dbCmd.Parameters.AddWithValue("@ManufacturerName", txtManufacturer.Text);
                    dbCmd.Parameters.AddWithValue("@BatchNumber", txtBatch.Text);
                    dbCmd.Parameters.AddWithValue("@ManufacturerDate", txtManuDate.Text);
                    dbCmd.Parameters.AddWithValue("@ProductEntryDate", txtEntryDate.Text);
                    dbCmd.Parameters.AddWithValue("@ExpireDate", txtExpiry.Text);
                    dbCmd.Parameters.AddWithValue("@ProductQuantity", txtExpiry.Text);
                    dbCmd.Parameters.AddWithValue("@ProductCostPrice", txtCost.Text);
                    dbCmd.Parameters.AddWithValue("@ProductTotalPrice", txtTotal.Text);
                    dbCmd.Parameters.AddWithValue("@SupplierId", txtSupplierId.Text);
                    dbCmd.Parameters.AddWithValue("@Product_ID", cbIdSelector.Text);

                    //execute update. Check to see how many rows were affected
                    int rowCount = dbCmd.ExecuteNonQuery();

                    //close database connection
                    dbConn.Close();

                    if (rowCount == 1)
                    {
                        MessageBox.Show("Record updated successfully");
                    }
                    else
                    {
                        MessageBox.Show("Error updating record. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmStockReport stockReport = new frmStockReport();
            stockReport.Show();
        }
    }
}
