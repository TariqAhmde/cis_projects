<?php
// Author: Tariq Ahmed
// Date: 2020-06-12
//Secure page
// start session index page
session_start();
//Secure the page
require_once('checkLoggedIn.php');
?>
<?php
//print_r($_SESSION);
//Set username from $_SESSION associative array
$userName = $_SESSION["username"];

//Set time zone for the page
$date = date_create("now", timezone_open("America/Halifax"));
$dateString = date_format($date, "Y/m/d H:iP");
$page = "home";

?>
<!DOCTYPE html>
<html>
<head>
    <title>Home - only visible if user gave us a username</title>
</head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<body>
<div id="container">
<?php require_once('nav.php');?>

    <?php

    // create connection to database
    @ $db = new mysqli('localhost', 'root', '', 'books');

    // if there is an error - handle it
    if (mysqli_connect_errno()) {
        echo 'Error: Could not connect to database.  Please try again later.</body></html>';
        exit;
    }

    // Get var from URL string
    // we can use addslashes to prevent injection or better yet
    // $searchy = addslashes($_GET['id']);
    //use mysqli_real_escape
    if (isset($_GET['id'])) {
        $searchy = $_GET['id'];
        $searchy = $db->real_escape_string($searchy);
        $query = "SELECT * FROM books WHERE title LIKE '%$searchy%';";
    } else {
        $query = "SELECT * FROM books;";
        $searchy = "";

    }

    // use query() method
    $result = $db->query($query);

    // gather number of rows we got in the query result set
    $num_results = $result->num_rows;

    echo "<p>Total Results: $num_results</p>";

    // if we have more than 0 rows
    if($result->num_rows > 0) {

    if ($searchy == "") {
        echo "We found ".$num_results." books in our database!";
    } else {
        echo "We found ".$num_results." books matching <span style='color:red;font-weight:bold'>".$searchy."</span> in our database!";
    }
    ?>
    <table class="table table-striped ">
        <tr>
            <th width="12%">Title</th>
            <th width="8%">Isbn</th>
            <th width="8%">Author</th>
            <th width="8%">Price</th>
        </tr>

        <?php
        //echo each row of book data
        while($row = $result->fetch_assoc()) {

            echo "<tr>
                        <td>".$row['title']."</td>
                        <td>".$row['isbn']."</td>
                        <td>".$row['author']."</td>
                        <td>".$row['price']." $"."</td>
                  </tr>";

        }
        } else {
            // if no rows echo out
            echo "We couldn't find anything for your matching term: ". stripslashes($searchy);
        }?>

    </table>
    <?php

    $result->free();
    // close database connection
    $db->close();
    ?>

</div>
</body>
</html>