<?php
// Author: Tariq Ahmed
// Date: 2020-06-12
// check for logged in session
if(!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn'])
{
    // user is not logged in
    // re-direct user to login.php
    header("Location: login.php?message=notLoggedIn");
    exit;
}