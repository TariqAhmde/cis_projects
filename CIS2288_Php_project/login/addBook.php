<?php
// Author: Tariq Ahmed
// Date: 2020-06-12
//Secure page
// start session
// Book Adding Functionality
session_start();
//Secure the page
require_once('checkLoggedIn.php');

// check for logged in session
if(!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn'])
{
    // user is not logged in
    // re-direct user to login_old.php
    header("Location: login.php?message=notLoggedIn");
    exit;
}

?>
<?php

$userName = $_SESSION["username"];

//Set time zone for the page
$date = date_create("now", timezone_open("America/Halifax"));
$dateString = date_format($date, "Y/m/d H:iP");
$page = "addBook";

?>
<!doctype html>
<html>
<head>
    <title>Add A New Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <h1>Book-O-Rama Book - Add Book</h1>
    <?php
    require_once("utilities.php");
    if (isset($_POST['submit'])) {

        // create short variable names
        $isbn = test_input($_POST['isbn']);
        $author = test_input($_POST['author']);
        $title = test_input($_POST['title']);
        $price = test_input($_POST['price']);

        if (empty($isbn) || empty($author) || empty($title) || empty($price)) {

            header("location:newBook.php?error=empty");
            exit();

        }
        //Create DB object
        require_once('config.php');

        $isbn = $mysqli->real_escape_string($isbn);
        $author = $mysqli->real_escape_string($author);
        $title = $mysqli->real_escape_string($title);
        $price = $mysqli->real_escape_string(doubleval($price));

        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.";
            exit;
        }

        $query = "INSERT INTO books VALUES (NULL,'". $isbn . "', '" . $author . "', '" . $title . "', " . $price . ")";
       // echo $query;
        $result = $mysqli->query($query);

        if ($result) {
            echo $mysqli->affected_rows . " book inserted into database. <a href='newBook.php'>Add another?</a>";

            //Display book inventory
            $query = "SELECT * FROM books";
            $result = $mysqli->query($query);

            $num_results = $result->num_rows;

            echo "<p>Number of books found: " . $num_results . "</p>";

            echo "<h2>CIS Book Inventory</h2>";
            echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
            if ($num_results > 0) {
                $books = $result->fetch_all(MYSQLI_ASSOC);
                echo "<table class='table table-bordered'><tr>";

//This dynamically retrieves header names
                foreach ($books[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }
                echo "</thead>";
                echo "<tbody>";
//Create a new row for each book
                foreach ($books as $book) {
                    echo "<tr>";

                    foreach ($book as $k => $v) {

                        echo "<td>" . $v . "</td>";

                    }
                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
            $result->free();
            $mysqli->close();
        } else {
            echo "An error has occurred.  The item was not added. <a href='newBook.php'>Try again?</a>";
        }

    } else {

        header("location:newBook.php?error=noform");
        exit();
    }

    ?>
    <p><a href="index.php">Go to Home</a> </p>

</div>
</body>
</html>
