<?php
// Author: Tariq Ahmed
// Date: 2020-06-12
// Navbar page
?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Book</a>
    </div>
    <ul class="nav navbar-nav">
      <li class='<?php if (isset($page) && $page == "home") {echo "active";}?>'><a href="index.php">Home</a></li>
      <li class='<?php if (isset($page) && $page == "addBook") {echo "active";}?>'><a href="addBook.php">Add</a></li>
      <li class='<?php if (isset($page) && $page == "viewBook") {echo "active";}?>'><a href="viewBooks.php">Edit and Delete</a></li>
    </ul>
      <div class="navbar-header navbar-right">
          <a class="navbar-brand" href="logout.php">Logout</a>
      </div>
  </div>
</nav>