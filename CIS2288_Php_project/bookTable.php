<?php
// Author: Tariq Ahmed
// Date: 2020-06-12
// Table Demo with login link
?>
<!doctype html>
<html>
    <head>
        <title>Book Search Results</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/custom.css">
    </head>
<body>
    <?php
        // create connection to database
        @ $db = new mysqli('localhost', 'root', '', 'books');

            // if there is an error - handle it
            if (mysqli_connect_errno()) {
                echo 'Error: Could not connect to database.  Please try again later.</body></html>';
                exit;
            }

            // Get var from URL string
            // we can use addslashes to prevent injection or better yet
            // $searchy = addslashes($_GET['id']);
            //use mysqli_real_escape
            if (isset($_GET['id'])) {
                $searchy = $_GET['id'];
                $searchy = $db->real_escape_string($searchy);
                $query = "SELECT * FROM books WHERE title LIKE '%$searchy%';";
            } else {
                $query = "SELECT * FROM books;";
                $searchy = "";

            }

            // use query() method
            $result = $db->query($query);

            // gather number of rows we got in the query result set
            $num_results = $result->num_rows;

    echo "<h2>Books</h2>";

    echo "<p>Total Books: $num_results</p>";

            // if we have more than 0 rows
            if($result->num_rows > 0) {

                if ($searchy == "") {
                    echo "We found ".$num_results." books in our database!";
                } else {
                    echo "We found ".$num_results." books matching <span style='color:red;font-weight:bold'>".$searchy."</span> in our database!";
                }
        ?>
                <table class="table table-striped">
            <tr>
                <th width="12%">Title</th>
                <th width="8%">Isbn</th>
                <th width="8%">Author</th>
                <th width="8%">Price</th>
            </tr>

            <?php
            //echo each row of book data
                while($row = $result->fetch_assoc()) {

            echo "<tr>
                        <td>".$row['title']."</td>
                        <td>".$row['isbn']."</td>
                        <td>".$row['author']."</td>
                        <td>".$row['price']." $"."</td>
                  </tr>";

                }
            } else {
                // if no rows echo out
                echo "We couldn't find anything for your matching term: ". stripslashes($searchy);
            }?>

             </table>
        <p><a href="login/login.php">Click here for login</a> </p>

        <?php

            $result->free();
            // close database connection
            $db->close();
          ?>
    </body>
</html>