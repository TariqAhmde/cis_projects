package info.hccis.flowershop.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.flowershop.entity.Customer;

@Database(entities = {Customer.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract CustomerDAO customerDAO();

}
