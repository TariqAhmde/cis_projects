package info.hccis.flowershop.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import info.hccis.flowershop.util.Util;


/**
 * This class will represent a camper.  It will be used for loading the data from the json string.
 * As well, it has the annotations needed to allow it to be used for the Room database.
 * @since 20200202
 * @author BJM
 */

@Entity(tableName = "customer")
public class Customer {

    public static final String CUSTOMER_BASE_API = Util.BASE_SERVER+"/api/CustomerService/";

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "customerTypeId")
    private Integer customerTypeId;
    @ColumnInfo(name = "fullName")
    private String fullName;
    @ColumnInfo(name = "address1")
    private String address1;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "province")
    private String province;
    @ColumnInfo(name = "postalCode")
    private String postalCode;
    @ColumnInfo(name = "phoneNumber")
    private String phoneNumber;
    @ColumnInfo(name = "birthDate")
    private String birthDate;
    @ColumnInfo(name = "loyaltyCard")
    private String loyaltyCard;
    @ColumnInfo(name = "customerTypeDescription")
    private String customerTypeDescription;


    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }

    public Customer()
    {
    }

    public Customer(Integer id)
    {
        this.id = id;
    }

    public Customer(Integer id, String fullName)
    {
        this.id = id;
        this.fullName = fullName;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getCustomerTypeId()
    {
        return customerTypeId;
    }

    public void setCustomerTypeId(Integer customerTypeId)
    {
        this.customerTypeId = customerTypeId;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getLoyaltyCard()
    {
        return loyaltyCard;
    }

    public void setLoyaltyCard(String loyaltyCard)
    {
        this.loyaltyCard = loyaltyCard;
    }



    @Override
    public String toString() {
        return "#"+id+" Name: "+this.fullName+","+" Birth Date: "+this.birthDate+","+" Phone No: "+this.phoneNumber;
    }

}
