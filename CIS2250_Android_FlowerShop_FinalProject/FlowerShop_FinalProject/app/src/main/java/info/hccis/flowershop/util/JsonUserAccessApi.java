package info.hccis.flowershop.util;

import java.util.List;

import info.hccis.flowershop.entity.UserAccess;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JsonUserAccessApi {

    /**
     * This abstract method to be created to allow retrofit to get list of users
     * @return List of users
     * @since 20200207
     * @author BJM (with help from the retrofit research.
     */

    @GET("user")
    Call<List<UserAccess>> getUsers();


    //Reference:  https://stackoverflow.com/questions/24100372/retrofit-and-get-using-parameters
    //Reference: https://howtodoinjava.com/retrofit2/query-path-parameters/
    @GET("login/{credentials}")
    Call<Integer> loginUser(@Path(value="credentials", encoded=true) String credentials);

}
