package info.hccis.flowershop.ui.customerdetail;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import info.hccis.flowershop.R;
import info.hccis.flowershop.entity.Camper;
import info.hccis.flowershop.entity.Customer;
import info.hccis.flowershop.ui.customerlist.CustomerListFragment;

public class CustomerDetailsFragment extends Fragment {

    private Camper camper;
    private Customer customer;
    private TextView tvFullName;
    private TextView tvAddress;
    private TextView tvCity;
    private TextView tvProvince;
    private TextView tvPostal;
    private TextView tvPhone;
    private TextView tvLoyaltyCard;
    private TextView tvDOB;
    private ImageButton buttonAddContact;
    private ImageButton buttonSendEmail;
    private OnFragmentInteractionListener mListener;


    public static CustomerDetailsFragment newInstance() {
        return new CustomerDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String customerJson = getArguments().getString("customer");
            Gson gson = new Gson();
            customer = gson.fromJson(customerJson, Customer.class);
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.customer_details_fragment, container, false);

        tvFullName = view.findViewById(R.id.textViewDetailFullName);
        tvAddress = view.findViewById(R.id.textViewDetailAddress);
        tvCity = view.findViewById(R.id.textViewDetailCity);
        tvProvince = view.findViewById(R.id.textViewDetailProvince);
        tvPostal = view.findViewById(R.id.textViewDetailPostal);
        tvPhone = view.findViewById(R.id.textViewDetailPhone);
        tvLoyaltyCard = view.findViewById(R.id.textViewDetailLoyalityCard);
        tvDOB = view.findViewById(R.id.textViewDetailDOB);

        tvFullName.setText("Full Name: "+customer.getFullName());
        tvAddress.setText("Address: "+customer.getAddress1());
        tvCity.setText("City: "+customer.getCity());
        tvProvince.setText("Province: "+customer.getProvince());
        tvPostal.setText("postal Code: "+customer.getPostalCode());
        tvPhone.setText("Phone Number: "+customer.getPhoneNumber());
        tvLoyaltyCard.setText("Loyalty card: "+customer.getLoyaltyCard());
        tvDOB.setText("Birth Date: "+customer.getBirthDate());


        buttonAddContact = view.findViewById(R.id.imageButtonAdd);

        /*
          BJM 20200203
          When the add button is clicked, will pass the camper object to the activity implementation
          method.  The MainActivity will then take care of adding the contact.
         */

        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(customer);
            }
        });

        /*
          BJM 20200203
          Similar to the add contact button, to send an email will notify the MainActivity and
          let it take care of sending the email with the information passed in the camper.
         */

        buttonSendEmail = view.findViewById(R.id.imageButtonSend);
        buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionSendEmail(customer);
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CustomerListFragment.OnListFragmentInteractionListener) {
            mListener = (CustomerDetailsFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionAddContact(Customer item);
        void onFragmentInteractionSendEmail(Customer customer);
    }
}
