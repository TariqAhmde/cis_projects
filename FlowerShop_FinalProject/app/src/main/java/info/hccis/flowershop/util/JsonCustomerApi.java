package info.hccis.flowershop.util;
import info.hccis.flowershop.entity.Camper;
import info.hccis.flowershop.entity.Customer;
import retrofit2.Call;
import java.util.List;
import retrofit2.http.GET;

public interface JsonCustomerApi {

    /**
     * This abstract method to be created to allow retrofit to get list of campers
     * @return List of campers
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("campers")
    Call<List<Camper>> getCampers();
    @GET("customers")
    Call<List<Customer>> getCustomers();
}
