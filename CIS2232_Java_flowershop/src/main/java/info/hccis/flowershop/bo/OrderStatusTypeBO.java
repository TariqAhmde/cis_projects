package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.OrderStatusTypeDAO;
import info.hccis.flowershop.entity.jpa.OrderStatusType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * OrderStatusType business object
 *
 * @author CIS2232
 * @since 20201016
 * @modified by Tariq Ahmed
 *
 */
public class OrderStatusTypeBO {

    private static HashMap<Integer, String> orderStatusTypesMap = new HashMap();

    public static HashMap<Integer, String> getOrderStatusTypesMap() {
        return orderStatusTypesMap;
    }

    public static void setOrderStatusTypesMap(HashMap<Integer, String> orderStatusTypesMap) {
        orderStatusTypesMap = orderStatusTypesMap;
    }

    /**
     * Connect to the data access object to get the orders from the data source.
     *
     * @since 20200923
     * @author BJM
     * @modified by Tariq Ahmed *
     */
    public ArrayList<OrderStatusType> load() {

        //Read from the database
        OrderStatusTypeDAO orderTypeDAO = new OrderStatusTypeDAO();
        ArrayList<OrderStatusType> orderStatusTypes = orderTypeDAO.select();

        return orderStatusTypes;
    }
}
