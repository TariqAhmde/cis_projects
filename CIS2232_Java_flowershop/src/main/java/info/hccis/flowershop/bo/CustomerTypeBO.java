package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerTypeDAO;
import info.hccis.flowershop.entity.jpa.Customertype;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * CustomerType business object
 *
 * @author CIS2232
 * @since 20201016
 * @modified by Tiago Viana *
 */
public class CustomerTypeBO {

    private static HashMap<Integer, String> customerTypesMap = new HashMap();

    public static HashMap<Integer, String> getCustomerTypesMap() {
        return customerTypesMap;
    }

    public static void setCustomerTypesMap(HashMap<Integer, String> customerTypesMap) {
        customerTypesMap = customerTypesMap;
    }

    /**
     * Connect to the data access object to get the customers from the
     * datasource.
     *
     * @return customerTypes
     * @since 20200923
     * @author BJM
     * @modified by Tiago Viana
     */
    public ArrayList<Customertype> load() {

        //Read from the database
        CustomerTypeDAO customerTypeDAO = new CustomerTypeDAO();
        ArrayList<Customertype> customerTypes = customerTypeDAO.select();

        return customerTypes;
    }
}
