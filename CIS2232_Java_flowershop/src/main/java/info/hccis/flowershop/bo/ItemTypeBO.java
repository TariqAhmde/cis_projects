package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.ItemTypeDAO;
import info.hccis.flowershop.entity.jpa.ItemType;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * ItemType business object
 *
 * @author CIS2232
 * @since 20201016
 * @ modified by Tariq Ahmed
 */
public class ItemTypeBO {

    private static HashMap<Integer, ItemType> itemTypesMap = new HashMap();

    public static HashMap<Integer, ItemType> getItemTypesMap() {
        return itemTypesMap;
    }

    public static void setItemTypesMap(HashMap<Integer, ItemType> itemTypesMap) {
        ItemTypeBO.itemTypesMap = itemTypesMap;
    }

    /**
     * Connect to the data access object to get the orders from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<ItemType> load() {

        //Read from the database
        ItemTypeDAO itemTypeDAO = new ItemTypeDAO();
        ArrayList<ItemType> itemTypes = itemTypeDAO.select();

        return itemTypes;
    }
}
