package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerDAO;
import info.hccis.flowershop.dao.FlowerOrderDAO;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import java.util.ArrayList;

/**
 * Flower Order business object
 *
 * @author CIS2232
 * @since 20201001
 */
public class FlowerOrderBO {

    /**
     * Select all records from the database
     *
     * @return List of the orders
     * @since 20200923
     * @author BJM
     * @modified by Tariq Ahmed
     */
    public ArrayList<FlowerOrder> selectAll() {

        //Read from the database
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        ArrayList<FlowerOrder> orders = orderDAO.selectAll();
        return orders;
    }

    /**
     * Insert the order into the database
     *
     * @since 20201001
     * @author BJM
     * @ modified by Tariq Ahmed Changed camper project into parking pass
     * project.
     */
    public boolean insert(FlowerOrder order) {
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.insert(order);
    }

    /**
     * Update the order into the database
     *
     * @since 20201001
     * @author BJM
     * @modified by Tariq Ahmed
     */
    public boolean update(FlowerOrder order) {
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.update(order);
    }

    /**
     * Delete the Order
     *
     * @since 20201009
     * @author BJM
     * @modified by Tariq Ahmed
     */
    public boolean delete(int id) {
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.delete(id);
    }

}
