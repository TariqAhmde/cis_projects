package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerDAO;
import info.hccis.flowershop.entity.jpa.Customer;
import java.util.ArrayList;

/**
 * Customer business object
 *
 * @author CIS2232
 * @since 20200923
 * @modified by Tiago Viana
 */
public class CustomerBO {

    /**
     * Connect to the data access object to get the customers from the
     * datasource.
     *
     * 
     * @since 20200923
     * @author BJM
     * @modified by Tiago
     */
    public ArrayList<Customer> load() {

        //Read customers from the database
        CustomerDAO customerDAO = new CustomerDAO();
        ArrayList<Customer> customers = customerDAO.select();

        return customers;
    }

    public Customer save(Customer customer) {
        CustomerDAO customerDAO = new CustomerDAO();

        //NOTE:  The id attribute generated from the database is an Integer not an
        //       int.  The default for an Integer is null so can't compare to 0.
        if (customer.getId() == null) {
            customer = customerDAO.insert(customer);
        } else {
            customerDAO.update(customer);
        }

        return customer;
    }

    /**
     * Delete the customer
     *
     * @since 20201009
     * @author BJM
     * @modified by Tiago *
     */
    public boolean delete(int id) {
        CustomerDAO customerDAO = new CustomerDAO();
        return customerDAO.delete(id);
    }

}
