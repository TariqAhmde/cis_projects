package info.hccis.flowershop;

import info.hccis.flowershop.rest.CustomerService;
import info.hccis.flowershop.rest.OrderService;
import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JerseyConfig extends ResourceConfig {

    @PostConstruct
    private void init() {
        registerClasses(CustomerService.class);
        registerClasses(OrderService.class);
    }
}
