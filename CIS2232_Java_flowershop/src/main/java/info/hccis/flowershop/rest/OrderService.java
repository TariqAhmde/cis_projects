package info.hccis.flowershop.rest;

import com.google.gson.Gson;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.repositories.OrderRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Order Service 
 *
 * @author bj
 * @since 20200923
 * @modified by Tariq Ahmed
 */

@Path("/OrderService/orders")
public class OrderService {

    private final OrderRepository or;

    @Autowired
    public OrderService(OrderRepository or) {
        this.or = or;
    }

    @GET
    @Produces("application/json")
    public ArrayList<FlowerOrder> getAll() {
        ArrayList<FlowerOrder> orders = (ArrayList<FlowerOrder>) or.findAll();
        return orders;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getFlowerOrderById(@PathParam("id") int id) throws URISyntaxException {

        Optional<FlowerOrder> orders = or.findById(id);

        if (!orders.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(orders).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createFlowerOrder(String orderJson) 
    {        
        try{
            String temp = save(orderJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
//    
//    @DELETE
//    @Path("/{id}")
//    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
//        Optional<Booking> booking = br.findById(id);
//        if(booking != null) {
//            br.deleteById(id);
//            return Response.status(HttpURLConnection.HTTP_CREATED).build();
//        }
//        return Response.status(404).build();
//    }
//
    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateFlowerOrder(@PathParam("id") int id, String orderJson) throws URISyntaxException 
    {

        try{
            String temp = save(orderJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }

    }

    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        FlowerOrder orders = gson.fromJson(json, FlowerOrder.class);
        
        if(orders.getCustomerId()== null) {
            throw new AllAttributesNeededException("Please provide all mandatory inputs");
        }
 
        if(orders.getId() == null){
            orders.setId(0);
        }

        orders = or.save(orders);

        String temp = "";
        temp = gson.toJson(orders);

        return temp;
        
        
    }
    
}
