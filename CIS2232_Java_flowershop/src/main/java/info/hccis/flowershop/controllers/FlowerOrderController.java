package info.hccis.flowershop.controllers;

import com.google.gson.Gson;
import info.hccis.flowershop.bo.FlowerOrderBO;
import info.hccis.flowershop.bo.ItemTypeBO;
import info.hccis.flowershop.bo.OrderStatusTypeBO;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.entity.jpa.ItemType;
import info.hccis.flowershop.repositories.CustomerRepository;
import info.hccis.flowershop.repositories.OrderRepository;
import info.hccis.flowershop.util.CisUtilityFile;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the order functionality of the site
 *
 * @since 20200930
 * @author CIS2232
 * @modified by Tariq Ahmed
 */
@Controller
@RequestMapping("/order")
public class FlowerOrderController {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    public FlowerOrderController(OrderRepository or, CustomerRepository cr) {
        orderRepository = or;
        customerRepository = cr;
    }

    /**
     * Page to allow user to view orders.
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project)
     * @modified by Tariq Ahmed
     */
    @RequestMapping("/list")
    public String list(Model model) {
        FlowerOrderBO orderBO = new FlowerOrderBO();

        //Go get the orders from the database.
        ArrayList<FlowerOrder> orders = orderBO.selectAll();
        //ArrayList<Booking> bookings = (ArrayList<Booking>) bookingRepository.findAll();

        model.addAttribute("orders", loadOrder());
        // Showing rows for parking passes
        model.addAttribute("findNameMessage", "Total Number of Orders loaded: " + orders.size() + "");

        return "order/list";
    }

    /**
     * Page to allow user to add an order
     *
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project
     * @modified by Tariq Ahmed
     */
    @RequestMapping("/add")

    public String add(Model model) {
        model.addAttribute("message", "Add an order");

        FlowerOrder order = new FlowerOrder();
        order.setId(0);
        model.addAttribute("order", order);

        //model.addAttribute("parkingpasses", loadParkingPass());
        ArrayList<FlowerOrder> found = (ArrayList<FlowerOrder>) orderRepository.findAll();

        // calculates row after added
        model.addAttribute("findNameMessage", "Total Number of Orders loaded: " + found.size() + "");

        //Load the names into the model
        Set<String> names = new HashSet();

        ArrayList<Customer> customers = (ArrayList<Customer>) customerRepository.findAll();
        for (Customer current : customers) {
            names.add(current.getFullName());
        }

        model.addAttribute("names", names);

        return "order/add";
    }

    /**
     * Page to allow user to submit the add an order. It will put the order in
     * the database.
     *
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project)
     * @ modified by Tariq Ahmed
     */
    @RequestMapping("/addSubmit")

    public String addSubmit(Model model, @Valid @ModelAttribute("order") FlowerOrder order, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "order/add";
        }

        // getting total cost but couldnt get item types cost
        double itemType = (order.getItem1() * 90 + order.getItem2() * 85 + order.getItem3() * 100 + order.getItem4() * 10 + order.getItem5() * 20 + order.getItem6() * 30);
        double cost = (itemType);
        order.setTotalCost(BigDecimal.valueOf(cost));

        orderRepository.save(order);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.order.saved");

        model.addAttribute("message", successAddString);

        model.addAttribute("orders", loadOrder());
        ArrayList<FlowerOrder> found = (ArrayList<FlowerOrder>) orderRepository.findAll();
        // calculates row 

        model.addAttribute("findNameMessage", "Total Number of orders loaded: " + found.size() + "");

        return "order/list";

    }

    /**
     * Page to allow user to edit a order
     *
     * @since 20201007
     * @author BJM (modified from Fred/Amro's project)
     * @modified by Tariq Ahmed
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        Optional<FlowerOrder> found = orderRepository.findById(id);

        //Load the names into the model
        Set<String> names = new HashSet();

        ArrayList<Customer> customers = (ArrayList<Customer>) customerRepository.findAll();
        for (Customer current : customers) {
            names.add(current.getFullName());
        }

        model.addAttribute("names", names);

        model.addAttribute("order", found);
        return "order/add";
    }

    /**
     * Page to allow user to delete a order
     *
     * @since 20201009
     * @author BJM
     * @modified by Tariq Ahmed
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);
        orderRepository.deleteById(id);

        ArrayList<FlowerOrder> found = (ArrayList<FlowerOrder>) orderRepository.findAll();
        // calculates row after deleted

        model.addAttribute("findNameMessage", "Total Number of orders loaded: " + found.size() + "");

        model.addAttribute("orders", found);
        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.order.deleted") + " (" + id + ")";

        model.addAttribute("message", successAddString);

        return "order/list";

    }

    @RequestMapping("/export")
    public String export(Model model) {

        File file = CisUtilityFile.setupFile(FlowerOrder.FILE_LOCATION);

        ArrayList<FlowerOrder> orders = loadOrder();
        ArrayList<String> ordersJson = new ArrayList();
        Gson gson = new Gson();
        for (FlowerOrder order : orders) {
            String orderString = gson.toJson(order);

            ordersJson.add(orderString);
        }
        for (String orderJson : ordersJson) {
            CisUtilityFile.write(orderJson, file);
        }
        model.addAttribute("orders", loadOrder());
        model.addAttribute("message", "Order list exported to file " + file.getAbsolutePath());

        return "order/list";
    }

    public ArrayList<FlowerOrder> loadOrder() {
        ArrayList<FlowerOrder> order = (ArrayList<FlowerOrder>) orderRepository.findAll();
        HashMap<Integer, String> orderStatusTypesMap = OrderStatusTypeBO.getOrderStatusTypesMap();
        for (FlowerOrder current : order) {
            current.setOrderStatusTypeDescription(orderStatusTypesMap.get(current.getOrderStatus()));
        }

        return order;

    }

}
