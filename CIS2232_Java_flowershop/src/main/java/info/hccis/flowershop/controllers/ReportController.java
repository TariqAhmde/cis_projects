package info.hccis.flowershop.controllers;

import info.hccis.flowershop.bo.OrderStatusTypeBO;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.repositories.CustomerRepository;
import info.hccis.flowershop.repositories.OrderRepository;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the report functionality of the site
 *
 * @since 20201021
 * @author CIS2232
 * @ modified by Tigao and Tariq
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;

    public ReportController(CustomerRepository cr, OrderRepository or) {
        customerRepository = cr;
        orderRepository = or;
    }

    /**
     * Page to allow user to specify customer
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project)
     * @ modified by Tiago
     */
    @RequestMapping("/orderByCusId")
    public String getFullName(Model model) {

        Customer customer = new Customer();
        model.addAttribute("customer", customer);

        FlowerOrder flowerorder = new FlowerOrder();
        model.addAttribute("flowerorder", flowerorder);

        return "report/orderByCusId";
    }

    /**
     * Page to allow user to submit the customer report
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project
     * @ modified by Tiago
     *
     */
    @RequestMapping("/submitId")
    public String addSubmit(Model model, @ModelAttribute("customer") Customer customer) {

        model.addAttribute("customers", loadCustomersById(customer.getId()));
        model.addAttribute("flowerorders", loadFlowerOrdersByCustomerId(customer.getId()));
        ArrayList<FlowerOrder> flowerorders = loadFlowerOrdersByCustomerId(customer.getId());
        BigDecimal ordersTotal = new BigDecimal(0);
        for (FlowerOrder flowerorder : flowerorders) {
            ordersTotal = ordersTotal.add(flowerorder.getTotalCost());
        }
        model.addAttribute("ordersTotal", ordersTotal);

        return "customer/listOrders";

    }

    public ArrayList<Customer> loadCustomersById(int id) {
        ArrayList<Customer> customers = (ArrayList<Customer>) customerRepository.findAllById(id);
        return customers;
    }

    public ArrayList<FlowerOrder> loadFlowerOrdersByCustomerId(int customerId) {
        ArrayList<FlowerOrder> flowerorders = (ArrayList<FlowerOrder>) orderRepository.findByCustomerId(customerId);
        HashMap<Integer, String> orderStatusTypesMap = OrderStatusTypeBO.getOrderStatusTypesMap();
        for (FlowerOrder current : flowerorders) {
            current.setOrderStatusTypeDescription(orderStatusTypesMap.get(current.getOrderStatus()));
        }
        return flowerorders;
    }

    /**
     * Page to allow user to specify order date
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project
     * @ modified by Tariq
     *
     */
    @RequestMapping("/orderDate")
    public String getOrderDate(Model model) {

        FlowerOrder order = new FlowerOrder();
        model.addAttribute("order", order);

        return "report/orderDate";
    }

    /**
     * Page to allow user to submit the order report
     *
     * @since 20201021
     * @author BJM (modified from Fred/Amro's project
     * @ modified by Tariq
     *
     */
    @RequestMapping("/submitDate")
    public String addSubmit(Model model, @ModelAttribute("order") FlowerOrder order) {

        model.addAttribute("orders", loadOrderByOrderDate(order.getOrderDate()));
        ArrayList<FlowerOrder> orders = loadOrderByOrderDate(order.getOrderDate());

        BigDecimal ordersTotal = new BigDecimal(0);
        for (FlowerOrder flowerOrder : orders) {
            ordersTotal = ordersTotal.add(flowerOrder.getTotalCost());
        }
        model.addAttribute("ordersTotal", ordersTotal);

        return "order/reportDateList";

    }

    public ArrayList<FlowerOrder> loadOrderByOrderDate(String orderDate) {
        ArrayList<FlowerOrder> order = (ArrayList<FlowerOrder>) orderRepository.findAllByOrderDate(orderDate);
        HashMap<Integer, String> orderStatusTypesMap = OrderStatusTypeBO.getOrderStatusTypesMap();
        for (FlowerOrder current : order) {
            current.setOrderStatusTypeDescription(orderStatusTypesMap.get(current.getOrderStatus()));
        }

        return order;

    }
}
