package info.hccis.flowershop.controllers;

import com.google.gson.Gson;
import info.hccis.flowershop.bo.CustomerTypeBO;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.repositories.CustomerRepository;
import info.hccis.flowershop.util.CisUtilityFile;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the Customer functionality of the site
 *
 * @since 20200930
 * @author CIS2232
 * @modified by Tiago Viana
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository cr) {
        customerRepository = cr;
    }

    /**
     * Page to allow user to view customers
     *
     * @param model
     * @return customer list page (view module)
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     * Modified by: Tiago Viana
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the customers from the database.
        //Use the jpa repository to get the customers.

        model.addAttribute("customers", loadCustomers());
        model.addAttribute("findNameMessage", "Customers loaded");

        return "customer/list";
    }
    
    /**
     *
     * @param model
     * @return customer list page (view module)
     * @Since 20201120
     * @author: Tiago Viana
     */
    @RequestMapping("/export")
    public String export(Model model) {
        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successString;
        try {
            File file = CisUtilityFile.setupFile(Customer.FILE_LOCATION);
        
        ArrayList<Customer> customers = loadCustomers();
        ArrayList<String> customersJson = new ArrayList();
        Gson gson = new Gson();
        for(Customer customer : customers) {
            String customerStr = gson.toJson(customer);

                //add camper to master list
                customersJson.add(customerStr);
        }
        for (String  customerJson : customersJson) {
            CisUtilityFile.write(customerJson, file);
        }
        successString = rb.getString("message.customer.exported") + " (" + file.getAbsolutePath() + ")";
        } catch ( Exception ex) {
            successString = rb.getString("message.customer.exported.error");
        }
        
        model.addAttribute("customers", loadCustomers());
        model.addAttribute("customerExportMessage", successString);

        return "customer/list";
    }

    /**
     * Page to allow user to add a customer
     *
     * @param model
     * @return add customer page (view module)
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project
     * @Modified by: Tiago Viana
     */
    @RequestMapping("/add")
    public String add(Model model) {

        model.addAttribute("message", "Add a customer");

        Customer customer = new Customer();
        model.addAttribute("customer", customer);

        return "customer/add";
    }

    /**
     * Page to allow user to submit the add a customer.It will put the customer in
 the database.
     *
     * @param model
     * @param customer
     * @param result
     * @return add customer page (view module)
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project
     * @Modified by: Tiago Viana
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("customer") Customer customer, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "customer/add";
        }

        customerRepository.save(customer);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.customer.saved");

        model.addAttribute("message", successAddString);
        model.addAttribute("customers", loadCustomers());

        return "customer/list";

    }

    /**
     * Page to allow user to edit a customer
     *
     * @param model
     * @param request
     * @return 
     * @since 20201007
     * @author BJM (modified from Fred/Amro's project
     * @Modified by: Tiago Viana
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);
        Optional<Customer> found = customerRepository.findById(id);
        model.addAttribute("customer", found);
        
        return "customer/add";
    }

    /**
     * Page to allow user to delete a Customer
     *
     * @param model
     * @param request
     * @return customer list page (view module)
     * @since 20201009
     * @author BJM
     * @Modified by: Tiago Viana
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successString;
        try {
            customerRepository.deleteById(id);
            successString = rb.getString("message.customer.deleted") + " (" + id + ")";
        } catch (EmptyResultDataAccessException e) {
            successString = rb.getString("message.customer.deleted.error") + " (" + id + ")";
        }

        model.addAttribute("message", successString);
        model.addAttribute("customers", loadCustomers());

        return "customer/list";
    }

    /**
     * @Since: 20201120
     * @Author: Tiago Viana
     * @return customers list.
     */
    public ArrayList<Customer> loadCustomers() {
        ArrayList<Customer> customers = (ArrayList<Customer>) customerRepository.findAll();
        HashMap<Integer, String> customerTypesMap = CustomerTypeBO.getCustomerTypesMap();
        for (Customer current : customers) {
            current.setCustomerTypeDescription(customerTypesMap.get(current.getCustomerTypeId()));
        }
        
        return customers;

    }

}
