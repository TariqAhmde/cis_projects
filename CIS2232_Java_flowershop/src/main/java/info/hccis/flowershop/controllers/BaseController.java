package info.hccis.flowershop.controllers;

import info.hccis.flowershop.bo.CustomerTypeBO;
import info.hccis.flowershop.bo.ItemTypeBO;
import info.hccis.flowershop.bo.OrderStatusTypeBO;
import info.hccis.flowershop.entity.jpa.Customertype;
import info.hccis.flowershop.entity.jpa.ItemType;
import info.hccis.flowershop.entity.jpa.OrderStatusType;
import info.hccis.flowershop.repositories.CustomerTypeRepository;
import info.hccis.flowershop.repositories.ItemTypeRepository;
import info.hccis.flowershop.repositories.OrderStatusTypeRepository;
import info.hccis.flowershop.util.CisUtility;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Base Class
 *
 * @since 20200930
 * @author bj
 * @modified by Tiago Viana and Tariq Ahmed
 *
 */
@Controller
public class BaseController {

    private final CustomerTypeRepository customerTypeRepository;
    private final OrderStatusTypeRepository orderStatusTypeRepository;
    private final ItemTypeRepository itemTypeRepository;

    public BaseController(CustomerTypeRepository ctr, OrderStatusTypeRepository ostr, ItemTypeRepository itr) {
        customerTypeRepository = ctr;
        orderStatusTypeRepository = ostr;
        itemTypeRepository = itr;

    }

    @RequestMapping("/")
    public String home(HttpSession session) {

        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate);

        ArrayList<Customertype> customerTypes = (ArrayList<Customertype>) customerTypeRepository.findAll();
        session.setAttribute("customerTypes", customerTypes);
        System.out.println("BJM, loaded " + customerTypes.size() + " customer types");

        HashMap<Integer, String> customerTypesMap = CustomerTypeBO.getCustomerTypesMap();
        customerTypesMap.clear();
        for (Customertype current : customerTypes) {
            customerTypesMap.put(current.getId(), current.getDescription());
        }
        
        // order status types
        ArrayList<OrderStatusType> orderStatusTypes = (ArrayList<OrderStatusType>) orderStatusTypeRepository.findAll();
        session.setAttribute("orderStatusTypes", orderStatusTypes);
        // System.out.println("BJM, loaded " + orderStatusTypes.size() + " Order status types");

        HashMap<Integer, String> orderStatusTypesMap = OrderStatusTypeBO.getOrderStatusTypesMap();
        orderStatusTypesMap.clear();
        for (OrderStatusType current : orderStatusTypes) {
            orderStatusTypesMap.put(current.getId(), current.getDescription());
        }

        // Item types description
        ArrayList<ItemType> itemTypes = (ArrayList<ItemType>) itemTypeRepository.findAll();
        session.setAttribute("itemTypes", itemTypes);
        session.setAttribute("itemTypesArray", itemTypes.toArray());
        System.out.println("BJM, loaded " + itemTypes.size() + " item types");

        HashMap<Integer, ItemType> itemTypesMap = new HashMap();
        itemTypesMap.clear();
        for (ItemType current : itemTypes) {
            itemTypesMap.put(current.getId(), current);
        }
        ItemTypeBO.setItemTypesMap(itemTypesMap);
        session.setAttribute("itemTypesMap", itemTypesMap);

        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
