package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.entity.jpa.OrderStatusType;
import info.hccis.flowershop.util.DatabaseUtility;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ParkingPass database access object
 *
 * @author bjm
 * @since 20201001
 * @ modified by Tariq
 */
public class FlowerOrderDAO {

    public static boolean checkConnection() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    /**
     * Select the records from the database
     *
     * @return
     * @since 20201001
     * @author BJM
     */
    public ArrayList<FlowerOrder> selectAll() {
        ArrayList<FlowerOrder> orders = new ArrayList();
        ArrayList<OrderStatusType> orderStatusTypes = new ArrayList();
        OrderStatusTypeDAO orderStatusTypeDAO = new OrderStatusTypeDAO();
        orderStatusTypes = orderStatusTypeDAO.select();

        //Select from the database
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            System.out.println("Could not make a connection to the database");
            return null;
        }

        try {

            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next selectAll all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from FlowerOrder");

            //Show all the orders
            while (rs.next()) {
                int id = rs.getInt("id");
                int customerId = rs.getInt("customerId");
                String orderDate = rs.getString("orderDate");
                int item1 = rs.getInt("item1");
                int item2 = rs.getInt("item2");
                int item3 = rs.getInt("item3");
                int item4 = rs.getInt("item4");
                int item5 = rs.getInt("item5");
                int item6 = rs.getInt("item6");
                int orderStatus = rs.getInt("orderStatus");
                BigDecimal totalCost = rs.getBigDecimal("totalCost");
                BigDecimal amountPaid = rs.getBigDecimal("amountPaid");

                FlowerOrder order = new FlowerOrder(id, customerId, orderDate, item1, item2, item3, item4, item5, item6, orderStatus, totalCost, amountPaid);

                for (OrderStatusType current : orderStatusTypes) {
                    if (current.getId().equals(orderStatus)) {
                        order.setOrderStatusTypeDescription(current.getDescription());
                    }
                }
                orders.add(order);
            }

        } catch (SQLException ex) {
            Logger.getLogger(FlowerOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return orders;

    }

    /**
     * Insert into the database.Note the id is expected to be 0.If it is not 0
     * then the update will be used.
     *
     * @param order
     * @return true if no exception occurs
     * @since 20201001
     * @author BJM
     */
    public boolean insert(FlowerOrder order) {

        Connection conn = null;

        try {
            //BJM 20200925 Removed the password.
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");
            return false;
        }

        if (order.getId() == 0) {

            //***************************************************
            // INSERT
            //***************************************************
            try {
                String theStatement = "INSERT INTO FlowerOrder(customerId, orderDate, item1, item2, item3, item4, item5, item6, orderStatus, totalCost, amountPaid) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setInt(1, order.getCustomerId());
                stmt.setString(2, order.getOrderDate());
                stmt.setInt(3, order.getItem1());
                stmt.setInt(4, order.getItem2());
                stmt.setInt(5, order.getItem3());
                stmt.setInt(6, order.getItem4());
                stmt.setInt(7, order.getItem5());
                stmt.setInt(8, order.getItem6());
                stmt.setInt(9, order.getOrderStatus());
                stmt.setBigDecimal(10, order.getTotalCost());
                stmt.setBigDecimal(11, order.getAmountPaid());

                stmt.executeUpdate();
                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
                return false;
            }
        } else {
            return update(order);
        }

    }

    /**
     * Update into the database.
     *
     * @param order
     * @return true if no exception occurs
     * @since 20201001
     * @author BJM
     */
    public boolean update(FlowerOrder order) {

        Connection conn = null;

        try {
            //BJM 20200925 Removed the password.
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");
            return false;
        }

        if (order.getId() == 0) {
            return false;
        } else {

            //***************************************************
            // update
            //***************************************************
            try {
                String theStatement = "UPDATE FlowerOrder set customerId=?, orderDate=?, item1=?, item2=?, item3=?, item4=?, item5=?, item6=?, orderStatus=?, "
                        + "totalCost=?, amountPaid=? where id=?";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setInt(1, order.getCustomerId());
                stmt.setString(2, order.getOrderDate());
                stmt.setInt(3, order.getItem1());
                stmt.setInt(4, order.getItem2());
                stmt.setInt(5, order.getItem3());
                stmt.setInt(6, order.getItem4());
                stmt.setInt(7, order.getItem5());
                stmt.setInt(8, order.getItem6());
                stmt.setInt(9, order.getOrderStatus());
                stmt.setBigDecimal(10, order.getTotalCost());
                stmt.setBigDecimal(11, order.getAmountPaid());
                stmt.setInt(12, order.getId());

                stmt.executeUpdate();
                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Delete the order into the database.
     *
     * @param id
     * @return
     * @since 20201009
     * @author BJM
     *
     */
    public boolean delete(int id) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM FlowerOrder WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

}
