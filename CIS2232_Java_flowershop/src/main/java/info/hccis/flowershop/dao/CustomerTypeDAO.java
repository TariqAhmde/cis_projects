package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.Customertype;
import info.hccis.flowershop.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Customer Type database access object
 *
 * @author bjm
 * @since 20201016
 * @ modified by Tiago
 *
 */
public class CustomerTypeDAO {

    /**
     * Select the records from the database
     *
     * @since 20201016
     * @author BJM
     */
    public ArrayList<Customertype> select() {
        ArrayList<Customertype> customerTypes = new ArrayList();

        Connection conn = null;
        try {

            //Select the customers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from customertype");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String description = rs.getString("description");

                Customertype customerType = new Customertype();
                customerType.setId(id);
                customerType.setDescription(description);
                customerTypes.add(customerType);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        for (Customertype customerType : customerTypes) {
            System.out.println("Test" + customerType.getDescription());
        }

        return customerTypes;

    }
}
