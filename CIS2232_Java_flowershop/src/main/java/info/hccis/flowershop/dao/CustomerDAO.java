package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.Customertype;
import info.hccis.flowershop.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Customer database access object
 *
 * @author bjm
 * @since 20200921
 * @ modified by Tiago
 *
 */
public class CustomerDAO {

    /**
     * Select the records from the database
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Customer> select() {
        ArrayList<Customer> customers = new ArrayList();

        ArrayList<Customertype> customerTypes = new ArrayList();
        CustomerTypeDAO camperTypeDAO = new CustomerTypeDAO();
        customerTypes = camperTypeDAO.select();

        Connection conn = null;
        try {

            //Select the campers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from customer");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                Integer customerTypeId = rs.getInt("customerTypeId");
                String fullName = rs.getString("fullName");
                String address1 = rs.getString("address1");
                String city = rs.getString("city");
                String province = rs.getString("province");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String birthDate = rs.getString("birtDate");
                String loyaltyCard = rs.getString("LoyaltyCard");

                Customer customer = new Customer(id);
                customer.setId(id);
                customer.setCustomerTypeId(customerTypeId);
                customer.setFullName(fullName);
                customer.setAddress1(address1);
                customer.setCity(city);
                customer.setProvince(province);
                customer.setPostalCode(postalCode);
                customer.setPhoneNumber(phoneNumber);
                customer.setBirthDate(birthDate);
                customer.setLoyaltyCard(loyaltyCard);

                for (Customertype current : customerTypes) {
                    if (current.getId().equals(customerTypeId)) {
                        customer.setCustomerTypeDescription(current.getDescription());
                    }
                }

                customers.add(customer);

            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return customers;

    }

    /**
     * Insert the customer into the database.
     *
     * @since 20200923
     * @author BJM
     *
     * 20200925 BJM Modifications to get running using standard password.
     */
    public Customer insert(Customer customer) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "INSERT INTO customer(customerTypeId,fullName,address1,city,province,postalCode,phoneNumber,birthDate,loyaltyCard)"
                    + "VALUES (?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, customer.getCustomerTypeId());
            stmt.setString(2, customer.getFullName());
            stmt.setString(3, customer.getAddress1());
            stmt.setString(4, customer.getCity());
            stmt.setString(5, customer.getProvince());
            stmt.setString(6, customer.getPostalCode());
            stmt.setString(7, customer.getPhoneNumber());
            stmt.setString(8, customer.getBirthDate());
            stmt.setString(9, customer.getLoyaltyCard());
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

        return customer;
    }

    /**
     * Update the customer into the database.
     *
     * @since 20201009
     * @author BJM
     *
     */
    public Customer update(Customer customer) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "UPDATE customer set customerTypeId=?,fullName=?,address1=?,city=?,province=?,postalCode=?,phoneNumber=?,birthDate=?,loyaltyCard=?"
                    + "WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, customer.getCustomerTypeId());
            stmt.setString(2, customer.getFullName());
            stmt.setString(3, customer.getAddress1());
            stmt.setString(4, customer.getCity());
            stmt.setString(5, customer.getProvince());
            stmt.setString(6, customer.getPostalCode());
            stmt.setString(7, customer.getPhoneNumber());
            stmt.setString(8, customer.getBirthDate());
            stmt.setString(9, customer.getLoyaltyCard());

            stmt.setInt(10, customer.getId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

        return customer;
    }

    /**
     * Delete the customer into the database.
     *
     * @since 20201009
     * @author BJM
     *
     */
    public boolean delete(int id) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

}
