package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.OrderStatusType;
import info.hccis.flowershop.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Order Status Type database access object
 *
 * @author bjm
 * @since 20201016
 * @ modified by Tariq
 *
 */
public class OrderStatusTypeDAO {

    /**
     * Select the records from the database
     *
     * @return
     * @since 20201016
     * @author BJM
     */
    public ArrayList<OrderStatusType> select() {
        ArrayList<OrderStatusType> orderStatusTypes = new ArrayList();

        Connection conn = null;
        try {

            //Select the orders from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from OrderStatusType");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String description = rs.getString("description");

                OrderStatusType orderType = new OrderStatusType();
                orderType.setId(id);
                orderType.setDescription(description);
                orderStatusTypes.add(orderType);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return orderStatusTypes;

    }
}
