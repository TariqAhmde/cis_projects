package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.Customertype;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerTypeRepository extends CrudRepository<Customertype, Integer> {
        ArrayList<Customer> findAllById(String name);
        ArrayList<Customer> findAllByDescription(String name);
        
}