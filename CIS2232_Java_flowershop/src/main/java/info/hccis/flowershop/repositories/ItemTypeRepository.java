package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.ItemType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemTypeRepository extends CrudRepository<ItemType, Integer> {
}