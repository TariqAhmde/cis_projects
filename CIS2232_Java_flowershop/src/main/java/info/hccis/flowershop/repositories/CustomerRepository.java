package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.Customer;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
        ArrayList<Customer> findAllById(int id);
        ArrayList<Customer> findAllByFullName(String fullName);

}