package info.hccis.flowershop.repositories;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.entity.jpa.OrderStatusType;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStatusTypeRepository extends CrudRepository<OrderStatusType, Integer> {
        ArrayList<FlowerOrder> findAllById(String name);
        ArrayList<FlowerOrder> findAllByDescription(String name);
}