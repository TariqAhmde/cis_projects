package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.FlowerOrder;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<FlowerOrder, Integer> {
    ArrayList<FlowerOrder> findByCustomerId(int customerId);
    ArrayList<FlowerOrder> findAllById(int id);
    ArrayList<FlowerOrder> findAllByOrderDate(String orderDate);

}